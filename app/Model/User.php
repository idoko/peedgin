<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = "user_id";

    protected $fillable = ['username', 'email', 'password', 'created_at'];

    protected $hidden = ['password', 'remember_token',];

    public function hasDownVotedDefinition($definitionId) {
        $vote = Vote::checkUserHasVote($this->user_id, $definitionId);
        if ($vote == false) {return false;};
        $voteType = $vote->vote_type;

        return ($voteType == Vote::$TYPE_DOWNVOTE);
    }

    public static function findByUsername($username) {
        $user = User::where('username', '=', $username);
        if ($user->get()->isEmpty()) {
            return null;
        }
        return $user->get()[0];
    }

    public function hasUpVotedDefinition($definitionId) {
        $vote = Vote::checkUserHasVote($this->user_id, $definitionId);
        if ($vote == false) {return false;};
        $voteType = $vote->vote_type;

        return ($voteType == Vote::$TYPE_UPVOTE);
    }

    public function getDefinitions() {
        return Definition::where('defined_by', '=', $this->user_id)
            ->orderBy('word_id', 'DESC')
            ->paginate(15);
    }

    public function isAdmin() {
        $rtv = Admin::where("user_id", "=", $this->user_id);
        return !($rtv->get()->isEmpty());
    }
}
