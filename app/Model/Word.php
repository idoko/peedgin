<?php
namespace App\Model;

use Carbon\Carbon;

class Word extends Model
{
    protected $table = "words";
    protected $primaryKey = "word_id";

    protected $fillable = ['word_id', 'word', 'views_today', 'added_by', 'added_date'];

    protected $dates = ['added_date'];

    public function getTopDefinition() {
        //this fuckard assumed that number of definitions
        //for a single word cannot be more than 1000, at least for now...
        $defs = $this->getDefinitions();
        $max = 0;
        //loop thru the array, if the definition score is above the current value of max,
        //set max to the score and set the topDef value to the definition object.
        foreach($defs as $def){
            if($def->score() > $max){
                $max = $def->score();
                $topDef = $def;
            }
        }
        return $topDef;
    }

    public static function fetchAll($sort) {
        switch ($sort) {
            case "alphabet":
                return self::select('*')
                    ->orderBy('word', 'ASC');
            break;
            case "popular":
                return self::select('*')
                    ->orderBy('num_of_views', 'DESC');
            break;
            case "new":
            default:
                return self::select('*')
                    ->orderBy('word_id', 'DESC');
            break;
        }
    }

    public function getAuthor() {
        return User::find($this->added_by);
    }

    public function getDefinitions() {
        $retval = Definition::where('word_id', '=', $this->word_id);
        /*if (count($retval) > 0) {
            return $retval->get();
        }
        return array();*/
        return $retval->get();
    }

    public static function getByTerm($term) {
        $retval = Word::where('word', '=', $term);
        if ($retval->get()->isEmpty()) {
            return null;
        }
        return $retval->get()[0];
    }

    public function getDefinitionCount() {
        return count($this->getDefinitions());
    }

    private function sortDefs(Definition $def1, Definition_Model $def2){
        if($def1->points == $def2->points){
            return 0;
        }
        else if($def1->points > $def2->points){
            return -1;
        }
        else{
            return 1;
        }
    }

    public function saveComponents(Definition $definition, $image = null, $examples = array()) {
        $retval = array();
        $definition->save();
        $retval['definition_id'] = $definition->definition_id;
        if ($image != null) {
            $image->definition_id = $definition->definition_id;
            $image->save();
            $retval['image_id'] = $image->id;
        }
        if (!empty($examples)) {
           foreach ($examples as $example) {
                $example->definition_id = $definition->definition_id;

                $example->save();
            }
        }
        return $retval;
    }

    private function getTimeSinceAdd() {
        $now = Carbon::now();
        $dt = new Carbon($this->added_date);
        $timePassed = $now->diffInHours($dt);
        return $timePassed;
    }
}