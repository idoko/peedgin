<?php
namespace App\Model;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class Definition extends Model
{
    protected $table = "definitions";
    protected $primaryKey = "definition_id";

    protected $fillable = ['word_id', 'definition_id', 'defined_by', 'definition_text'];

    const UPVOTE_OFFSET = 8;
    const DOWNVOTE_OFFSET = 15;

    public function getAuthor() {
        return User::find($this->defined_by);
    }

    public function hasImage() {
        $retval = DefinitionImage::where('definition_id', '=', $this->definition_id)->first();
        return $retval;
    }

    public function getImage() {
        return DefinitionImage::where('definition_id', '=', $this->definition_id)->first();
    }
    public function hasExamples() {
        $retval = Usage::where('definition_id', '=', $this->definition_id)->first();
        return $retval;
    }

    public function getExamples() {
        return Usage::where('definition_id', '=', $this->definition_id)->get();
    }

    public function getParentWord() {
        return Word::find($this->word_id);
    }

    public function getVotes() {
        $retval = Vote::where('definition_id', '=', $this->definition_id);
        if ($retval->get()->isEmpty()) {
            return array();
        }
        return $retval->get();
    }

    public function getUpVotes() {
        $retval = Vote::where('definition_id', '=', $this->definition_id)
            ->where('vote_type', '=', 1);
        if ($retval->get()->isEmpty()) {
            return array();
        }
        return $retval->get();
    }

    public function getDownVotes() {
        $retval = Vote::where('definition_id', '=', $this->definition_id)
            ->where('vote_type', '=', 0);
        if ($retval->get()->isEmpty()) {
            return array();
        }
        return $retval->get();
    }

    public function score() {
        $ups = 0; $downs = 0;
        $data = $this->getVotes();
        foreach ($data as $vote) {
            if ($vote->vote_type == 0) {
                $downs++;
            } else if($vote->vote_type == 1) {
                $ups++;
            } else {

            }
        }

        $score = $ups + $this::UPVOTE_OFFSET / ($ups+$this::UPVOTE_OFFSET) + ($downs  + $this::DOWNVOTE_OFFSET);
        return ceil($score);
    }


    public static function matchDefinition($query) {
        $ret = [];
        $term = Word::where('word', '=', $query);
        //if the query exists in our directory, just show the word
        if (!$term->get()->isEmpty()) {
            $ret[] = $term->get()[0];
        } else {
            $sql = "SELECT DISTINCT words.word, words.word_id
            FROM words LEFT JOIN definitions
            ON definitions.word_id = words.word_id
            WHERE (words.word = :query1) OR MATCH (definitions.definition_text) AGAINST (:query2)
            OR MATCH(words.word) AGAINST (:query3)";
            $params = [':query1' => $query, ':query2' => $query, ':query3' => $query];
            $results = DB::select($sql, $params);
            if (empty($results)) {
                return $results;
            }
            foreach ($results as $result) {
                //result is an instance of stdClass...so, typecast to array.
                $ret[] = new Word((array)$result);
            }
        }
        return $ret;
    }
}