<?php
namespace App\Model;

class DefinitionImage extends Model
{
    protected $table = "images";

    protected $primaryKey = 'id';

    protected $fillable = ['file_name', 'mime_type', 'definition_id'];

    public function getDefinition() {
        return Definition::find($this->definition_id);
    }
}