<?php
namespace App\Model;

class Vote extends Model
{
    public static $TYPE_UPVOTE = 1;
    public static $TYPE_DOWNVOTE = 0;
    public static $TYPE_UPVOTE_STRING = "upvote";
    public static $TYPE_DOWNVOTE_STRING = "downvote";

    protected $table = "votes";

    protected $primaryKey = "vote_id";

    protected $fillable = ['vote_type', 'voted_by', 'definition_id'];

    public function getType() {
        if ($this->vote_type == self::$TYPE_DOWNVOTE) {
            return self::$TYPE_DOWNVOTE_STRING;
        }
        return self::$TYPE_UPVOTE_STRING;
    }

    public function toggleType() {
        if ($this->vote_type == self::$TYPE_UPVOTE) {
            $this->vote_type = self::$TYPE_DOWNVOTE;
        } else {
            $this->vote_type = self::$TYPE_UPVOTE;
        }
        $this->save();
    }

    public static function checkUserHasVote($userId, $definitionId) {
        $model = Vote::where('voted_by', '=', $userId)
            ->where('definition_id', '=', $definitionId);
        if ($model->get()->isEmpty()) {
            return false;
        }
        return $model->get()[0];
    }
}