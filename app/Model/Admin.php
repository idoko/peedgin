<?php
namespace App\Model;

class Admin extends Model
{
    protected $table = "admins";

    protected $fillable = ["user_id", "password"];
}