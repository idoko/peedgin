<?php
namespace App\Model;

class Flag extends Model
{
    protected $table  = "flags";
    protected $primaryKey = "id";
    /* item id holds the id of the flagged entry in its parent table
    * item table holds the table from which the entry originates
     */
    protected $fillable =  ["id", "item_id", "added_by", "item_table", "reason"];

    const TYPE_WORD = "words";
    const TYPE_DEFINITION = "definitions";
    const TYPE_IMAGE = "images";
    const TYPE_USAGE = "usages";
}