<?php
namespace App\Model;

class Usage extends Model
{
    protected $table = "usages";
    protected $primaryKey = "usage_id";

    protected $fillable = ['definition_id', 'usage_text'];
}