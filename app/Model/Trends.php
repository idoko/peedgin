<?php
namespace App\Model;

use Illuminate\Support\Facades\DB;

class Trends extends Model
{
    protected $fillable = ['record_date', 'most_viewed_views', 'tenth_most_viewed_views',
            ];
    protected $dates = ['record_date'];

    protected $table = "trends";

    public static function getTodayTrends() {
        return Word::select('*')
            ->skip(0)
            ->take(10)
            ->get();
    }
}