<?php
namespace App\Http\Controllers;

use App\Model\Definition;
use App\Model\Vote;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VoteController
{
    public function doAddVote(\Illuminate\Http\Request $request) {
        $voteExists = Vote::checkUserHasVote(Auth::user()->user_id, $request->input('definition_id'));
        if ($voteExists) {
            $oldVote = $voteExists;
            if ($oldVote->vote_type == $request->input('vote_type')) {
                $oldVote->delete();
            } else {
                //toggle the vote type
                $oldVote->vote_type  = ($oldVote->vote_type == 0) ? 1 : 0;
                $oldVote->save();
            }
            return redirect()->back();
        } else {
            $rules = [
                'vote_type' => 'integer|required|max:1',
                'definition_id' => 'integer|required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back();
            }
            $def = Definition::find($request->input('definition_id'));
            if ($def != null) {
                $vote = Vote::create([
                    'vote_type' => $request->input('vote_type'),
                    'voted_by' => Auth::user()->user_id,
                    'definition_id' => $def->definition_id
                ]);
                $vote->save();
                return redirect()->back();
            }
        }
    }
}