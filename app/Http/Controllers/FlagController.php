<?php
namespace App\Http\Controllers;

use App\Model\Flag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class FlagController extends Controller
{
    public function flagItem(Request $request) {
        $rules = [
            "type" => "in:words,definitions,images,usages|required",
            "iid"=>"int|required",
            "user"=>"int",
            "reason"=>"string"
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            Session::flash('alert-message', "Failed to complete request");
        }
        $flag = new Flag(
            ["item_id"=>$request->input('iid'),
                "added_by" => Auth::user()->user_id,
                "item_table" => $request->input('type'),
                "reason" => $request->input('reason')
        ]);
        $flag->save();
        return redirect()->back();
    }
}