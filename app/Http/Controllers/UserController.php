<?php
namespace App\Http\Controllers;

use App\Model\User;

class UserController extends Controller
{
    public function showLogin() {
        $data = [
            'showSearch' => false
        ];
        return view('user.login', $data);
    }

    public function showRegForm() {
        $data = [
            'showSearch' => false
        ];
        return view('user.register', $data);
    }

    public function showDefinitions($username) {
        $user = User::findByUsername($username);
        $data['user'] = $user;
        $data['definitions'] = $user->getDefinitions();
        return view('user.definitions', $data);
    }
}