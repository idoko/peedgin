<?php
namespace App\Http\Controllers;

use App\Model\Definition;
use App\Model\DefinitionImage;
use App\Model\Trends;
use App\Model\Word;
use App\Model\Usage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class WordController extends Controller
{
    public static $DEFINITION_IMAGE_PATH = "/images/def_img";

    public function index() {
        $order = (isset($_GET['order'])) ? $_GET['order'] : "";
        $words = Word::fetchAll($order)->paginate(10)
            ->appends(Input::except('page'));
        $user = Auth::user();
        $data = array();
        $data['words'] = $words;
        $data['user'] = $user;
        $data['trending'] = Trends::getTodayTrends();

        return view('word.index', $data);
    }

    /*
     * This paginates an instance of Eloquent Collections so no need to call
     * $model->get() in the underlying model
     */
    public function search(Request $request) {
        $query = $request->get('query');
        if ($query != null) {
            $results = Definition::matchDefinition($query);
            $data['results'] = $results;
            $data['query'] = $query;
            return view('word.search', $data);
        }
    }

    public function showTerm($word) {
        $term = Word::where('word', '=', $word);
        if ($term->get()->isEmpty()) {
            //return word not defined
            Session::flash('alert-message', $word." has not been defined yet. Got any idea? Help us add it.");
            return redirect()->route('add-term');
        } else {
            $data = array();
            $term = $term->get()[0];
            $term->increment('num_of_views');
            $term->increment('views_today');
            $definitions = $term->getDefinitions();
            $data['term'] = $term;
            $data['definitions'] = $definitions;
            $data['related'] = array_slice(Definition::matchDefinition($definitions[0]->definition_text),
                0, 10);
            $data['user'] = Auth::user();
            if ($term->getTopDefinition()->hasImage()) {
                $imgFile = $term->getTopDefinition()->getImage()->file_name;
                $data['og_image'] = route('home')."/images/def_img/".$imgFile;
            } else {
                $data['og_image'] = "";
            }

            return view('word.view', $data);
        }
    }

    public function showNewDefinition($term) {
        $term = Word::getByTerm($term);
        if ($term == null) {
            echo "word doesn't exist yet";
            //redirect to new word page
        } else {
            $data['term'] = $term;
            return view('word.new_definition', $data);
        }
    }

    public function showAddWord() {
        return view('word.add');
    }

    public function doAddWord(Request $request)  {
        $rules = [
            'title' => 'string|required',
            'definition' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)
                ->withInput();
        }
        if (($word = Word::getByTerm($request->input('title'))) == null) {
            $word = new Word(['word' => $request->input('title'),
                'added_by' => Auth::user()->user_id]);
            $word->save();
        }
        $definition = $this->buildDefinition(['definition_text' => $request->input('definition'),
            'word_id' => $word->word_id,
            'defined_by' => Auth::user()->user_id,
            'added_date' => new \DateTime()]);
        $image = null; $usages = array();
        if ($request->hasFile('definition_image')) {
            $filename = $this->saveFile($request->input('title'), $request->file('definition_image'));
            $image = $this->buildDefinitionImage(['filename' => $filename,
                'mime_type' => 'image/jpeg']);
        }
        $examples = Input::get('examples');
        if ($examples != null && !empty($examples)) {
            foreach ($examples as $example) {
                if (trim($example) != "") {
                    $example = $this->buildDefinitionExample(['usage_text' => $example]);
                    $usages[] = $example;
                }
            }
        }
        $word->saveComponents($definition, $image, $usages);
        return redirect()->route('define', $word->word);
    }

    public function doNewDefinition(Request $request) {
        $rules = [
            'meaning' => 'required',
            'word' => 'integer|required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)
                ->withInput();
        }
        $word = Word::find($request->input('word'));
        if ($word == null) {
            $errors = new MessageBag(['error' => ['No matching word was found']]);
            return Redirect::back()->withErrors($errors);
        }
        $definition = $this->buildDefinition(['definition_text' => $request->input('meaning'),
            'word_id' => $word->word_id,
            'defined_by' => Auth::user()->user_id,
            'added_date' => new \DateTime()]);
        $image = null; $usages = array();
        if ($request->hasFile('definition_image')) {
            $filename = $this->saveFile($word->word, $request->file('definition_image'));
            $image = $this->buildDefinitionImage(['filename' => $filename,
                'mime_type' => 'image/jpeg']);
        }
        $examples = Input::get('examples');
        if ($examples != null && !empty($examples)) {
            foreach ($examples as $example) {
                if (trim($example) != "") {
                    $example = $this->buildDefinitionExample(['usage_text' => $example]);
                    $usages[] = $example;
                }
            }
        }
        $word->saveComponents($definition, $image, $usages);
        //var_dump($usages);
        return redirect()->route('define', $word->word);
    }

    private function buildDefinition($data = array()) {
        $definition = new Definition([
            'definition_text' => self::makeHyperLink($data['definition_text']),
            'word_id' => $data['word_id'],
            'defined_by' => $data['defined_by'],
            'added_date' => $data['added_date'],
        ]);
        return $definition;
    }

    private static function makeHyperLink($text) {
        $text = htmlspecialchars($text);
        //don't match if space exists between starting '}' and first word
        //or last word and ending '{'
        $pattern = "/{([a-zA-Z0-9\s]+)\w}/";
        preg_match_all($pattern, $text, $matches);
        $mentions = $matches[0];

        if(!empty($mentions)){
            foreach($mentions as $mention){
                //first remove the surrounding curly brackets
                //$word = str_replace("}", "", $mention);
                $word = substr($mention, 1, -1);
                $url = "<a href=".route('define', $word).">$word</a>";
                $text = str_replace($mention, $url, $text);
            }
        }
        return $text;
    }

    private function buildDefinitionImage($data = array()) {
        $defImage = new DefinitionImage([
            'file_name' => $data['filename'],
            'mime_type' => $data['mime_type'],
        ]);
        return $defImage;
    }

    private function buildDefinitionExample($data = array())  {
        $example = new Usage([
            'usage_text' => self::makeHyperLink($data['usage_text'])
        ]);
        return $example;
    }



    private function saveFile($title, $file) {
        $filename = substr(sha1($title), 0, 30) . "." . $file->guessExtension();
        if (!$file->move(public_path(self::$DEFINITION_IMAGE_PATH), $filename)) {
            $errors = new MessageBag(['error' => ['Failed to complete file upload']]);
            return Redirect::back()->withErrors($errors);
        }
        return $filename;
    }

}