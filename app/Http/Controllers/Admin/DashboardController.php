<?php
namespace App\Http\Controllers\Admin;

use App\Model\Word;
use Illuminate\Support\Facades\Auth;

class DashboardController
{
    public function showDashboard() {
        $data = [
            'user' => Auth::user(),
        ];
        return view('admin.index', $data);
    }

    public function showWords() {
        $data = [
            'user' => Auth::user(),
            'words' => Word::all(),
        ];
        return view('admin.words_index', $data);
    }

    public function viewWord($word) {
        $term = Word::where('word', '=', $word);
        if (!$term->get()->isEmpty()) {
            $term = $term->get()[0];
            $data = [
                'user' => Auth::user(),
                'word' => $term,
                'definitions' => $term->getDefinitions(),
            ];
            return view('admin.view_word', $data);
        } else {
            return "No such word was found";
        }
    }
}