$(document).ready(function () {

    //voting
    $("button[class^='vote-']").on('click', function (event) {
        event.preventDefault();
        var def = $(this).siblings('#def').val(),
            action = $(this).parent('form').attr('action'),
            url = action+"&async=true",
            type = $(this).val();
        if($('#fred').val() == ""){
            fetchPage(url, $('.modal-dialog'));
        }
        else{
            $.post(url, {vote: 'Vote', type: type, definition: def}, function (data) {
                if(data.success == null){
                    fetchPage(url, $('.modal-dialog'));
                }
            }, 'json').success(function (data) {
                if(data.success == true){
                    location.reload();
                }
                else{
                    $('.content').prepend('<div class="col-5 centered message-neutral">' +
                        'Failed to complete your request');
                }
            })
        }
    });
});