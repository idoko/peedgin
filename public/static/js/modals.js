var overlay = $('.overlay');
var closeButtonElement = "<a href='#' class='modal-close'>" +
    "<span class='mdi mdi-close'></span> </a>",
    closeButton = $('.modal-close'),
    modalIsOpen = false;

overlay.append(closeButtonElement);


function fetchPage(page, parentElement){

    openModal(parentElement);
    parentElement.empty().append('<div class="message-neutral dataContainer"></div>');
    var container = $('.dataContainer');
    container.empty().append("<img src='public/static/img/loader.gif' class='loader'/>");
    //$('body').addClass('modal-is-open');

    var jqxhr = $.get(page, function () {

    })
        .done(function (data) {
            container.empty().append(data);
        })
        .fail(function () {
            container.empty().append("Failed to complete request");
        });
}

function closeModal(modal){
    var overlay = $('.overlay');
    if(modalIsOpen){
        modal.hide();
        overlay.hide();
        $('body').removeClass('modal-is-open');

    }
}

function openModal(modal){

    var overlay = $('.overlay'); //somehow the global variable ain't working here. will look later
    overlay.append(closeButtonElement).css("display", "block");
    //overlay.show();
    modal.show(300);
    modal.css("position", "absolute");
    modalIsOpen = true;

    var closeButton = $('.modal-close');
    closeButton.on("click", function (e) {
        e.preventDefault();
        closeModal(modal);
    });

    //close with escape key
    $(document).keyup(function (e) {
        if(e.which == '27'){
            closeButton.click();
        }
    });

    //close by clicking outside the element
    $('body').mouseup(function (e) {
        //if the click target is not modal and is not a descendant of modal
       if(!modal.is(e.target) && modal.has(e.target).length === 0){
           closeButton.click();
       }
    });
}

function scrollList(ul){

}
