<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WordController@index')->name('home');

/*
 | User auth & pages
 */
Route::get('/login', 'UserController@showLogin')->name('login');
Route::post('/login', 'Auth\LoginController@doUserLogin')->name('do-login');
Route::get('/register', 'UserController@showRegForm')->name('register');
Route::post('/register', 'Auth\RegisterController@doUserRegistration')->name('do-register');
Route::get('/author/{username}', function($username) {
    $ctrl = new \App\Http\Controllers\UserController();
    return $ctrl->showDefinitions($username);
})->name('author');
/*
 | word stuffs
 */
Route::get('/add-term', 'WordController@showAddWord')
    ->middleware('auth')->name('add-term');
Route::post('/add-term', 'WordController@doAddWord')
    ->middleware('auth')->name('do-add-term');
Route::get('/{term}/new-definition', function ($term) {
    $ctrl = new \App\Http\Controllers\WordController();
    return $ctrl->showNewDefinition($term);
})->middleware('auth')->name('new-definition');
Route::post('/new-definition', 'WordController@doNewDefinition')->middleware('auth')->name('do-new-definition');
Route::get('define/{term}', function($term) {
    $ctrl = new \App\Http\Controllers\WordController();
    return $ctrl->showTerm($term);
})->name('define');

Route::get('/vote/subscribe', function() {
    return redirect()->route('home');
});
Route::post('/vote/subscribe', 'VoteController@doAddVote')
    ->middleware('auth')
    ->name('vote');

/*
 | Search
 */
Route::get('/search', 'WordController@search')->name('search');

/*
 | Flags
 */
Route::post('/flags/', 'FlagController@flagItem')
    ->middleware('auth')->name('flag');
Route::get('/flags/', function() {
    return redirect('/');
});

/*
 |Administration
 */
Route::group(["namespace" => "Admin", "middleware" => "admin"], function() {
    Route::get("/admin", "DashboardController@showDashboard")
        ->name('admin');

    Route::get("/admin/words", "DashboardController@showWords")
        ->name("admin-words");

    Route::get('/admin/word/{term}', function($term) {
        $controller = new App\Http\Controllers\Admin\DashboardController();
        return $controller->viewWord($term);
    })->name('view-word');
});