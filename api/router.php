<?php
function call($controller, $action){

    switch($controller){
        case 'user':
            $controller = new \api\controller\UserController();
            break;
        case 'word':
            $controller = new \api\controller\WordController();
            break;
        default: //set default to error
            $controller = new \api\controller\ErrorController();
            break;
    }

    //check if method exists first
    if(method_exists($controller, $action))
    {
        $controller->{$action}();
    }
    else{
        $controller = new \api\controller\ErrorController();
        $controller->{$action}();
    }
}
//the $controller variable is passed from index.php
if(!ctype_alnum($controller)){
    $controller = new \api\controller\ErrorController();
}
call($controller, $action);