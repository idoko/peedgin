<?php
namespace api\model;

use model\User_Model as ParentUserModel;

class UserModel extends ParentUserModel
{
	public function pack() {
		return get_object_vars($this);
	}

	public function buildFromParent(ParentUserModel $userModel) {
		$this = $userModel;
	}
}