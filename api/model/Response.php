<?php
namespace api\model;


class Response
{
    private $success;
    private $errorCode;
    private $errorMessage;
    private $content;

    public function __construct() {}

    public function setSuccessStatus(bool $status) {
        $this->success = $status;
        return $this;
    }

    public function setMessage(string $message) {
        $this->errorMessage = $message;
        return $this;
    }

    public function setCode(int $code) {
        $this->errorCode = $code;
        return $this;
    }

    public function setContent($content) {
        $this->content = $content;
        return this;
    }

    public function pack() {
        return get_object_vars($this);
    }
}