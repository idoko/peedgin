<?php
namespace api\controller;

class ErrorController extends Controller
{
    public function invalid() {
        self::invalidate();
    }

    public static function invalidate($code=400, $message="Bad request") {
        $response = array("success"=>false, "error_code"=>$code,
            "message"=>$message);
        echo json_encode($response);
        return -1;
    }
}