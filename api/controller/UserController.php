<?php
namespace api\controller;

use api\model\Response;
use api\model\UserModel;

class UserController extends Controller
{
    public function authenticate() {
        if (empty($_POST)) {
            ErrorController::invalidate();
        }
        $username = isset($_POST['username']) ? $_POST['username'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";

        $response = new Response();
        if (trim($username) == "" || trim($password) == "") {
            $response->setSuccessStatus(false)
                ->setCode(422)
                ->setMessage("Username and/or password cannot be blank");
            echo json_encode($response->pack());
            return -1;
        } else {
            $data = array('username' => $username);
            $temp = new UserModel($data);
            $temp->setPasswordFromPost($password);
            try {
                $user = $temp->login();
                $user = new UserModel(array());
                $user->buildFrom
                $response->setSuccessStatus(true)
                    ->setCode(200)
                    ->setMessage("Authentication successful");
                $response->setContent($user->pack());
                echo json_encode($response->pack());
                return 0;
            } catch (\Exception $e) {
                $response->setSuccessStatus(false)
                    ->setCode(422)
                    ->setMessage("Incorrect username and/or password");
                error_log($e->getMessage());
                echo json_encode($response->pack());
                return -1;
            }
        }
    }
}