<?php
ob_start();

include_once __DIR__."/../vendor/autoload.php";

$ROOT_PATH = "";

require_once __DIR__.$ROOT_PATH."/../assets/kofare_config.php";

require_once __DIR__.$ROOT_PATH."/../assets/db_config.php";

require_once __DIR__.$ROOT_PATH."/../helper/functions.php";

$controller = isset($_GET['controller']) ? $_GET['controller'] : "error";
$action = isset($_GET['action']) ? $_GET['action'] : "invalid";

header("Content-Type: application/json");

include_once __DIR__."/router.php";