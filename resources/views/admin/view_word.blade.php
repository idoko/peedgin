@extends('admin.layout.fixed')
@section('content')
  <section class="content">
    <div class="row">
    <div class="col-md-8">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>

          <h3 class="box-title">Definitions for {{$word->word}}</h3>
        </div>
        @if (!empty($definitions))
          @foreach($definitions as $definition)
            <div class="box-body">

              <dl class="dl-horizontal">
                {{$definition->definition_text}}
              </dl>
        </div>
          @endforeach
          @endif
      </div>
      <!-- /.box -->
    </div>
    </div>
  </section>
@endsection