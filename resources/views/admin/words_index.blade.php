@extends('admin.layout.fixed')

@section('styles')
  <link rel="stylesheet" href="/sudo/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Word ID</th>
                  <th>Word</th>
                  <th>Added By</th>
                  <th>Created On</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if (!empty($words))
                  @foreach($words as $word)
                    <tr>
                      <td>{{$word->word_id}}</td>
                      <td>{{$word->word}}</td>
                      <td>{{$word->getAuthor()->username}}</td>
                      <td>2017-01-01 12:30 AM</td>
                      <td>View | Delete</td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>Word ID</th>
                  <th>Word</th>
                  <th>Added By</th>
                  <th>Created On</th>
                  <th>Actions</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection


@section('scripts')
  <!-- DataTables -->
  <script src="/sudo/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/sudo/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script>
      $(function () {
          $('#example1').DataTable();
          $('#example2').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
          })
      })
  </script>
@endsection