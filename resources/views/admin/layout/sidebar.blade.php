<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="/sudo/img/armadilo.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{$user->username}}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!--
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
      </div>
    </form> -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li>
        <a href="{{route('admin')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Flags</span>
          <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Media</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Words</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Definitions</a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Users</a></li>
        </ul>
      </li>
      <li>
        <a href="{{route('admin-words')}}">
          <i class="fa fa-th"></i> <span>Words</span>
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Users</span>
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>Definition Requests</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>