<ul>
  <li>
    <a href="x"
       title="Add new meaning" class="more">
      <img src="/images/comment-plus-outline.png" alt="Add new Definition" />
    </a>
  </li>
  &bull;
  <li class="facebook">
    <a href="{{$facebookLink}}" target="_blank" title="Share on Facebook" class="facebook">
      <img src="/images/facebook.png" alt="Share to Facebook"/>
    </a>
  </li>
  <li class="twitter">
    <a href="{{$twitterLink}}" target="_blank" title="Share on Twitter" class="twitter">
      <img src="/images/twitter.png" alt="Post to Twitter"/>
    </a>
  </li>
  <li>
    <a href="{{$whatsappLink}}" target="_blank"  title="Share on Whatsapp" class="whatsapp">
      <img src="/images/whatsapp.png" alt="Share on Whatsapp"/>
    </a>
  </li>
</ul>