<!DOCTYPE html>
<html>
<head>
  <meta property="og:url" content="@yield('ogUrl', '')" />
  <meta property="og:type" content="@yield('ogType', '')" />
  <meta property="og:description" content="@yield('ogDescription', '')" />
  <meta property="og:title" content="@yield('ogTitle', 'Know Your Pidgin - The community-curated Pidgin Dictionary')" />
  <meta property="og:image" content="@yield('ogImage', '')" />
  <base href="/" />
  <meta charset="utf-8" />
  <meta name="theme-color" content="#bf0b2d">
  <title>@yield('title')</title>
  <link rel="icon" href="/favico.png" type="image/png">
  <meta name="description" content="@yield('pageDescription',
      "Reviving African languages, one word at a time and bringing you the meaning of every term, slang and phrase that exists in Nigerian or Ghanaian pidgin english - as defined by the community.")">
  <meta name="keywords" content="Nigerian pidgin, West africa, pidgin english, dictionary, pidgin meaning, Nigerian slangs, street language" />
  <meta name=viewport content="width=device-width, initial-scale=1">
  <link href="/fonts/material/css/materialdesignicons.min.css" rel="stylesheet" media="all" />
  <link href="/css/app.css" rel="stylesheet" />
  <link href="/css/navigation.css" rel="stylesheet" />
  <link href="/css/style.css" rel="stylesheet" />
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container">
    <div class="col-sm-2 col-md-2">
      <div class="col-sm-4 col-md-4 navbar-header">
        <a class="navbar-brand" href="/" title="Peedgin!">
          <img src="/images/logo300x300.png" alt="Peedgin Logo">
        </a>

      </div>
    </div>
    <div class="le-navigation">
      <ul class="nav navbar-nav" itemscope="" itemtype="http://www.schema.org/SiteNavigationElement">
        <li itemprop="name"><a itemprop="url" href="/?order=alphabet" title="Browse">Browse</a></li>
        <li itemprop="name"><a itemprop="url" href="#" title="Word of the Day">WOTD</a></li>
        <li itemprop="name"><a itemprop="url" href="#" title="About Peedgin">About Us</a></li>
        <li itemprop="name"><a itemprop="url" href="{{route('add-term')}}" title="Add new word">Add a Word</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container" style="">
  @if (isset($showSearch) && $showSearch === false)
  @else
  <div class="row">
  <div class="col-xs-10 col-xs-offset-1">
  <form class="col-sm-6 col-md-6 navbar-form" role="search" method="get" action="{{route('search')}}" style="border: transparent;">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search" name="query">
          <div class="input-group-btn">
            <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
  @endif
  @yield('content')
</div>
</body>
</html>