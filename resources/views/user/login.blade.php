@extends('layouts.skeleton')
@section('title', 'User Login :: Peedgin!')
@section('content')
  <div class="col-md-4 col-md-offset-4" style="margin-top: 20vh;">

    @if ($errors->any())
      @foreach($errors->all() as $error)
        <div class="bg-danger alert">{{$error}}</div>
      @endforeach
    @endif
    @foreach(['error', 'warning', 'message'] as $msg)
        @if(Session::has('alert-'.$msg))
          <div class="bg-{{$msg}} alert">{{Session::get('alert-'.$msg)}}</div>
        @endif
      @endforeach
    <form action="{{route('do-login')}}" method="post">
      <div class="form-group">
        <label>Username: </label>
        <input type="text" class="form-control" name="username" placeholder="Username" />
      </div>
      <div class="form-group">
        <label>Password: </label>
        <input type="password" class="form-control" name="password" placeholder="Password" />
      </div>
      <input type="submit" name="login" value="Login" class="btn btn-primary">
      {{csrf_field()}}
    </form>
    <div class="row">
      <p style="text-align: center;">Not registered?
        <a href="{{route('register')}}"> Create an account in 3 seconds</a>
      </p>
    </div>
  </div>
@endsection