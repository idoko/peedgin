@extends('layouts.skeleton')
@section('title', 'Join the Peedgin Community!')
@section('content')
  <div class="col-md-4 col-md-offset-4" style="margin-top: 20vh;">

    @if ($errors->any())
      @foreach($errors->all() as $error)
        <div class="bg-danger alert">{{$error}}</div>
      @endforeach
    @endif
    <form action="{{route('do-register')}}" method="post">
      <div class="form-group">
        <label>Email: </label>
        <input type="text" class="form-control" name="email" placeholder="Email Address" />
      </div>
      <div class="form-group">
        <label>Username: </label>
        <input type="text" class="form-control" name="username" placeholder="Username" />
      </div>
      <div class="form-group">
        <label>Password: </label>
        <input type="password" class="form-control" name="password" placeholder="Password" />
      </div>
      <input type="submit" name="login" value="Sign Up" class="btn btn-primary">
      {{csrf_field()}}
    </form>
    <div class="row">
      <p style="text-align: center;">Already registered?
        <a href="{{route('login')}}"> Login to your account</a>
      </p>
    </div>
  </div>
@endsection