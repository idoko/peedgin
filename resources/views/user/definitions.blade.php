@section('title', "Definitions by $user->username | Peedgin!")
@extends('layouts.skeleton')
@section('content')


  @if (count($definitions) > 1)
    <div class="pageIntro" style="margin-top: -16px; padding: 12px;">
      <h3>definitions by <span class="wordTitle">{{$user->username}}</span></h3>
    </div>
    <div class="col-md-8">
      @foreach($definitions as $definition)
        <div class="row" style="background-color: #fff; margin-bottom: 16px; border-radius: 4px;">
          <div class="word">
            <div class="col-xs-1 definitionVotes">
              <form action="{{route('vote')}}" method="post">
                <input type="hidden" name="definition_id" value="{{$definition->definition_id}}" />
                <button  name="vote_type" type="submit" value="1" class="vote-upvotes" >
                  {{count($definition->getUpVotes())}}
                  @if ($user != null &&
                      $user->hasUpVotedDefinition($definition->definition_id))
                    <span>
                  <img src="/images/thumb-up-active.png" alt="Upvote" />
                </span>
                  @else
                    <span>
                  <img src="/images/thumb-up-passive.png" alt="Upvote" />
                </span>
                  @endif
                </button>
                <input type="hidden" name="definition_id" value="{{$definition->definition_id}}" />
                <button name="vote_type" type="submit" value="0" class="vote-downvotes">
                  @if ($user != null &&
                      $user->hasDownVotedDefinition($definition->definition_id))
                    <span>
                  <img src="/images/thumb-down-active.png" alt="Downvote"/>
                </span>
                  @else
                    <span>
                  <img src="/images/thumb-down-passive.png" alt="Downvote"/>
                </span>
                  @endif
                  {{count($definition->getDownVotes())}}
                </button>
                {{csrf_field()}}
              </form>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-10 definition-content">
              <h3 class=""><a href="{{route('define', $definition->getParentWord()->word)}}">
                  {{$definition->getParentWord()->word}}</a></h3>
              <p class="definition-content">
                {{$definition->definition_text}}
              </p>
            </div>
            @if ($definition->hasImage())
              <div class="col-xs-12 definitionImage" style=" text-align: center;">
                <img src="{{'/images/def_img/'.$definition->getImage()->file_name}}" />
              </div>
            @endif
            <div class="col-xs-12" style=" text-align: left;">
              <div style="padding: 12px;">
                <div class="iconsets-horizontal">
                <span class="author" style="font-size: 1em; font-style: italic;">Added by <a
                    href="{{route('author', $definition->getAuthor()->username)}}">
                        {{$definition->getAuthor()->username}}
                    </a>
                </span>
                  <ul>
                    <li>
                      <a href="{{route('new-definition', $definition->getParentWord()->word)}}"
                         title="Add new meaning" class="more">
                        <img src="/images/comment-plus-outline.png" alt="Add new Definition" />
                      </a>
                    </li>
                    &bull;
                    <li class="facebook">
                      <a href="#" target="_blank" title="Share on Facebook" class="facebook">
                        <img src="/images/facebook.png" alt="Share to Facebook"/>
                      </a>
                    </li>
                    <li class="twitter">
                      <a href="#" target="_blank" title="Share on Twitter" class="twitter">
                        <img src="/images/twitter.png" alt="Post to Twitter"/>
                      </a>
                    </li>
                    <li>
                      <a href="#" target="_blank"  title="Share on Whatsapp" class="whatsapp">
                        <img src="/images/whatsapp.png" alt="Share on Whatsapp"/>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

        </div>
      @endforeach
      <div class='row page-wrap'>
        {{$definitions->links()}}
      </div>
    </div>
    <div class="col-md-4" style="">
      <div style="background-color: #fff; min-height: 120px;padding: 12px">

      </div>
    </div>
  @endif
@endsection