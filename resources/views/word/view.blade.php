@section('title', "Definitions for $term->word at Peedgin!")
@section('ogUrl', route('define', $term->word))
@section('ogDescription', $term->getTopDefinition()->definition_text)
@section('ogTitle', $term->word."'s meaning on Peedgin!")
@section('ogImage', $og_image)

@extends('layouts.skeleton')
@section('content')

  <div class="row">
    <section class="col-md-8 parent">

      <div class="col-md-10">
        <div class="pageIntro" style="padding: 8px;">
          <h3>definitions for <span class="wordTitle">{{$term->word}}</span>
          </h3>
        </div>
        @foreach($definitions as $definition)
          <div class="row" style="background-color: #fff; margin-bottom: 16px; border-radius: 4px;">
            <div class="word">
              <div class="col-xs-1 definitionVotes">
                <form action="{{route('vote')}}" method="post">
                  <input type="hidden" name="definition_id" value="{{$definition->definition_id}}" />
                  <button  name="vote_type" type="submit" value="1" class="vote-upvotes">
                    {{count($definition->getUpVotes())}}
                    @if ($user != null &&
                      $user->hasUpVotedDefinition($definition->definition_id))
                      <span>
                  <img src="/images/thumb-up-active.png" alt="Upvote" />
                </span>
                    @else
                      <span>
                  <img src="/images/thumb-up-passive.png" alt="Upvote" />
                </span>
                    @endif
                  </button>
                  <input type="hidden" name="definition_id" value="{{$definition->definition_id}}" />
                  <button name="vote_type" type="submit" value="0" class="vote-downvotes">
                    @if ($user != null &&
                        $user->hasDownVotedDefinition($definition->definition_id))
                      <span>
                  <img src="/images/thumb-down-active.png" alt="Downvote"/>
                </span>
                    @else
                      <span>
                  <img src="/images/thumb-down-passive.png" alt="Downvote"/>
                </span>
                    @endif
                    {{count($definition->getDownVotes())}}
                  </button>
                  {{csrf_field()}}
                </form>
              </div>
              <div class="col-xs-1"></div>
              <div class="col-xs-10 definition-content">
                @if (\Illuminate\Support\Facades\Auth::user())
                <form action="{{route('flag')}}" method="post">
                  <input type="hidden" name="iid" value="{{$definition->definition_id}}" />
                  <input type="hidden" name="type" value="{{\App\Model\Flag::TYPE_DEFINITION}}" />
                  <input type="hidden" name="user" value="{{\Illuminate\Support\Facades\Auth::user()->user_id}}">
                  {{csrf_field()}}
                  <p>
                  <button type="submit" class="pull-right" style="cursor: pointer; clear: both;
                  background-color: transparent; border: none;">
                    <img src="/images/flag.png" alt="Flag" width="15" height="18"/>
                  </button>
                  </p>
                </form>
                @endif
                <p class="definition">{!! $definition->definition_text !!}</p>
              </div>
              @if ($definition->hasImage())
                <div class="col-xs-12 definitionImage" style=" text-align: center;">
                  <img src="{{'/images/def_img/'.$definition->getImage()->file_name}}" />
                </div>
              @endif
              @if ($definition->hasExamples())
                <div class="col-xs-10">
                  <ol>
                    @foreach($definition->getExamples() as $example)
                      <li style="list-style: square;"><i>{!! $example->usage_text !!}</i></li>
                    @endforeach
                  </ol>
                </div>
              @endif
              <div class="col-xs-12" style=" text-align: left;">
                <div style="padding: 12px;">
                  <div class="iconsets-horizontal">
                <span class="author" style="font-size: 1em; font-style: italic;">Added by <a
                    href="{{route('author',$definition->getAuthor()->username)}}">
                        {{$definition->getAuthor()->username}}
                    </a>
                </span>
                    @php
                      $whatsappLink = "whatsapp://send?text=".route('define', $term->word);
                      $facebookLink = generateLink("https://facebook.com", "/sharer/sharer.php?u=",
                        route('define', $term->word));
                      $twitterLink = generateLink("https://twitter.com", "/share?url=",
                        route('define', $term->word)."&text=Checkout out \"$term->word\" on Peedgin!&hashtags=peedgin,pidgin,naijalingo");
                    @endphp
                    @include("layouts.sharer")
                  </div>
                </div>
              </div>
            </div>

          </div>
        @endforeach

        <span class="btn btn-large addButton">
                <a href="{{route('new-definition', $term->word)}}">Add New</a>
            </span>
      </div>
    </section>
    <section class="col-md-4" style="background: #fff; min-height: 100px;">
      @if ($related != null)
        @foreach($related as $relative)
          <h4 style="margin: 16px 0;"><a href="{{route('define', $relative->word)}}">{{$relative->word}}</a></h4>
        @endforeach
      @endif
    </section>
  </div>
  <style type="text/css">
    .pageIntro{
      margin-bottom:1em;
    }
    .addButton{
      background-color: #202a3c;
      border: none;
      border-radius:4px;
      padding: 5px 10px;
    }
    .addButton a, .addButton a:hover{
      text-decoration: none;
      color: #fff;
    }
    .wordTitle{
      font-style: italic;
      font-weight:bold;
    }
    .definition-content{
      margin: .8em 0;
      font-size: 1.3em;
    }
    .parent{
      padding: 0;
    }
  </style>
@endsection