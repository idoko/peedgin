@section('title', "$query | Peedgin Search")
@extends('layouts.skeleton')
@section('content')

  <div class="col-md-8">
    <div class="pageIntro" style="margin-top: -16px; padding: 12px;">
      <h3>Search results for <span class="wordTitle">"{{$query}}"</span></h3>
    </div>
    @if (!empty($results))
      <ul class="row" style="list-style-type: none;">
        @foreach($results as $result)
          <li class="word" style="background-color: #fff; margin-bottom: 16px; border-radius: 4px; padding: 12px;">
            <h3><a href="{{route('define', $result->word)}}">{{$result->word}}</a></h3>
            <p class="passive">
              @if ($result->getDefinitionCount() < 2)
                {{$result->getDefinitionCount()}} Definition
              @else
                {{$result->getDefinitionCount()}} Definitions
              @endif
            </p>
          </li>
        @endforeach
      </ul>
    @else
      <div class="">
        <h3><b>Nothing exists here...Want to help?</b></h3>
        <a href="#" style="color: #fff;text-align: center;">
          <button type="button" class="glossButton-medium" style="margin-top: 1em;cursor:pointer;">
            Add a new definition</button>
        </a>
      </div>
    @endif
  </div>
@endsection
