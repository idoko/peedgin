@section('title', 'Peedgin! - The community driven Pidgin Dictionary')
@extends('layouts.skeleton')

@section('content')
  
  @if (count($words) > 1)
    <div class="col-md-8">
      @foreach($words as $word)
        <div class="row" style="background-color: #fff; margin-bottom: 16px; border-radius: 4px;">
          <div class="word">
            <div class="col-xs-1 definitionVotes">
              <form action="{{route('vote')}}" method="post">
                <input type="hidden" name="definition_id" value="{{$word->getTopDefinition()->definition_id}}" />
                <button  name="vote_type" type="submit" value="1" class="vote-upvotes" >
                  {{count($word->getTopDefinition()->getUpVotes())}}
                  @if ($user != null &&
                      $user->hasUpVotedDefinition($word->getTopDefinition()->definition_id))
                    <span>
                  <img src="/images/thumb-up-active.png" alt="Upvote" />
                </span>
                  @else
                    <span>
                  <img src="/images/thumb-up-passive.png" alt="Upvote" />
                </span>
                  @endif
                </button>
                <input type="hidden" name="definition_id" value="{{$word->getTopDefinition()->definition_id}}" />
                <button name="vote_type" type="submit" value="0" class="vote-downvotes">
                  @if ($user != null &&
                      $user->hasDownVotedDefinition($word->getTopDefinition()->definition_id))
                    <span>
                  <img src="/images/thumb-down-active.png" alt="Downvote"/>
                </span>
                  @else
                    <span>
                  <img src="/images/thumb-down-passive.png" alt="Downvote"/>
                </span>
                  @endif
                  {{count($word->getTopDefinition()->getDownVotes())}}
                </button>
                {{csrf_field()}}
              </form>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-10 definition-content">
              <h3 class=""><a href="{{route('define', $word->word)}}">{{$word->word}}</a></h3>
              <p class="definition-content">
                {!!$word->getTopDefinition()->definition_text !!}
              </p>
            </div>
            @if ($word->getTopDefinition()->hasImage())
              <div class="col-xs-12 definitionImage" style=" text-align: center;">
                <img src="{{'/images/def_img/'.$word->getTopDefinition()->getImage()->file_name}}" />
              </div>
            @endif
            <div class="col-xs-12" style=" text-align: left;">
              <div style="padding: 12px;">
                <div class="iconsets-horizontal">
                <span class="author" style="font-size: 1em; font-style: italic;">Added by <a
                    href="{{route('author', $word->getTopDefinition()->getAuthor()->username)}}">
                        {{$word->getTopDefinition()->getAuthor()->username}}
                    </a>
                </span>
                  <ul>
                    <li>
                      <a href="{{route('new-definition', $word->word)}}"
                         title="Add new meaning" class="more">
                        <img src="/images/comment-plus-outline.png" alt="Add new Definition" />
                      </a>
                    </li>
                    &bull;
                    @php
                      $whatsappLink = "whatsapp://send?text=".route('define', $word->word);
                      $facebookLink = generateLink("https://facebook.com", "/sharer/sharer.php?u=",
                        route('define', $word->word));
                      $twitterLink = generateLink("https://twitter.com", "/share?url=",
                        route('define', $word->word)."&text=Checkout out \"$word->word\" on Peedgin!&hashtags=peedgin,pidgin,naijalingo");
                    @endphp
                    <li class="facebook">
                      <a href="{{$facebookLink}}" target="_blank" title="Share on Facebook" class="facebook">
                        <img src="/images/facebook.png" alt="Share to Facebook"/>
                      </a>
                    </li>
                    <li class="twitter">
                      <a href="{{$twitterLink}}" target="_blank" title="Share on Twitter" class="twitter">
                        <img src="/images/twitter.png" alt="Post to Twitter"/>
                      </a>
                    </li>
                    <li>
                      <a href="{{$whatsappLink}}" target="_blank"  title="Share on Whatsapp" class="whatsapp">
                        <img src="/images/whatsapp.png" alt="Share on Whatsapp"/>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

        </div>
      @endforeach
      <div class='row page-wrap'>
        {{$words->links()}}
      </div>
    </div>
    <div class="col-md-4" style="">
      <div style="background-color: #fff; min-height: 120px;padding: 12px">
        <h4 style="font-weight: bold;padding: 12px 0; border-bottom: 1px solid #202a3c" class="section-title">Trending</h4>
        @if($trending != null)
          @foreach($trending as $trend)
            <h4 style="margin: 16px 0;"><a href="{{route('define', $trend->word)}}">{{$trend->word}}</a></h4>
          @endforeach
        @endif
      </div>
    </div>
  @endif
@endsection