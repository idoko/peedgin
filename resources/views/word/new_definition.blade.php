@section('title', "New definition for ".$term->word." | Peedgin!")
@extends('layouts.skeleton')

@section('content')
  <section class="col-md-8 col-md-offset-1">
    <h3 class="section-title">
      Add new meaning for <em>{{$term->word}}</em>
    </h3>


    @if ($errors->any())
      @foreach($errors->all() as $error)
        <div class="bg-danger alert">{{$error}}</div>
      @endforeach
    @endif

    <p style="color: #1d4c74;">
      <span class="mdi mdi-information"></span> Link to an entry by putting it between curly brackets e.g "{duro}".
    </p>

    <form action="{{route('do-new-definition')}}" method="post" id="def_form" enctype="multipart/form-data">
      <div class="form-group">
        <label for="meaning">Meaning: </label>
        <textarea name="meaning" placeholder="Meaning" id="meaning" class="form-control" value="{{old('meaning')}}" style="height: 120px;"></textarea>
      </div>
      <div class="form-group">
        <label for="photo"></label>
        <input type="file" name="definition_image" id="photo" class="form-control" accept="image/*"/>
      </div>
      <input type="hidden" name="word" value="{{$term->word_id}}" />
      <div class="form-group">
        <label for="examples">Example Usages:</label>
        <textarea name="examples[]" placeholder="Sample Usage" class="form-control"></textarea>
      </div>
      <div class="form-group">
        <label for="examples">Example Usages:</label>
        <textarea name="examples[]" placeholder="Sample Usage" class="form-control"></textarea>
      </div>
      {{csrf_field()}}
      <button type="submit" name="add" class="btn btn-primary">Submit</button>
    </form>
  </section>
  <script type="text/javascript">
      //show the selected filename after photo has been selected
      $('#photo').on("change",function(event){
          $('#filename').empty().append($(this).val().split('\\').pop());
      });
  </script>
@endsection