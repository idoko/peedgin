@section('title', 'Add a new word | Peedgin')
@extends('layouts.skeleton')
@section('content')
  <section class="col-md-8 col-md-offset-1">
    <h3 class="section-title">
      Create a new word!
    </h3>


    @if ($errors->any())
      @foreach($errors->all() as $error)
        <div class="bg-danger alert">{{$error}}</div>
      @endforeach
    @endif
    @foreach(['error', 'warning', 'message'] as $msg)
      @if(Session::has('alert-'.$msg))
        <div class="bg-{{$msg}} alert">{{Session::get('alert-'.$msg)}}</div>
      @endif
    @endforeach
    <p style="color: #1d4c74;">
      <span class="mdi mdi-information"></span> Link to an entry by putting it between curly brackets e.g "{duro}".
    </p>
    <form action="{{route('do-add-term')}}" method="post" id="def_form" enctype="multipart/form-data">
      <div class="form-group">
        <label for="term">Word:</label>
        <input type="text" class="form-control" autofocus="" name="title" placeholder="Term to define" id="term" />
      </div>
      <div class="form-group">
        <label for="definition">Meaning: </label>
        <textarea name="definition" class="form-control" placeholder="Meaning" id="definition" style="height: 20vh;"></textarea>
      </div>
      <div class="form-group">

        <label for="photo"></label>
        <input type="file" name="definition_image" id="photo" class="form-control"/>
      </div>
      <div class="row" id="example-wrap">
        <label for="examples">Example Usages:</label>
        <div class="form-group">
          <textarea name="examples[]" placeholder="Sample Usage" class="example form-control"></textarea>
        </div>
        {{csrf_field()}}
        <div class="form-group">
          <textarea name="examples[]" placeholder="Sample Usage" class="example form-control"></textarea>
        </div>
      </div>
      <button type="submit" name="create" class="btn btn-primary">Submit</button>
    </form>
  </section>
@endsection